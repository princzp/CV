A more or less up to date Curriculum Vitae of Peter J. Princz and a more or less generic cover letter for job applications.

Both are written in LaTeX and to be pretty printed to pdfs with pdflatex.
There is a GNU makefile to build the pdf files and fire up a pdf reader on them. Should work on Linux.
All source files are provided in the src/ directory, and all the included files in the inc/ directory.

For the benefit of those who cannot build it with gnu make and pdflatex for whatever reason, the bin/ directory contains all the generated files.
You need the pdf files from there only.