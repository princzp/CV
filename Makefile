.PHONY: clean edit pdf view build all
.DEFAULT_GOAL := pdf

all: clean edit pdf view
build: clean pdf view

view:
	# evince ./bin/*.pdf &
	# mupdf ./bin/cv_pp*.pdf &
	# mupdf ./bin/cover_letter_pp*.pdf &
	texworks ./bin/*.pdf &

pdf:
	pdflatex -output-directory ./bin ./src/sample_cover_letter_pp_yyyy_mm_dd.tex
	# pdflatex -output-directory ./bin/ ./src/cover_letter_pp*.tex 
	if [ -f ./src/cover_letter_pp*.tex ] ; then \
		pdflatex -output-directory ./bin ./src/cover_letter_pp*.tex ; \
	fi ;
	pdflatex -output-directory ./bin ./src/cv_pp*.tex

edit:
	# gvim -p -f ./src/cover_letter_pp*.tex ./src/cv_pp*.tex
	# gvim -p -f ./src/*.tex
	gvim -p ./src/*.tex

clean:
	# rm -f ./bin/cover_letter_pp*
	# rm -f ./bin/cv_pp*
	rm -r ./bin
	mkdir -p ./bin ./bin/appendix
	cp -p ./inc/*.pdf ./bin/appendix/
